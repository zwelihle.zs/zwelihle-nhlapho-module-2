class App {
  App(String a, String b, String c, int d) {
    this.appName = a;
    this.sector = b;
    this.developer = c;
    this.year = d;
  }

  allCap() {
    return this.appName?.toUpperCase();
  }

  String? appName;
  String? sector;
  String? developer;
  int? year;
}

void main() {
  App ambani = App(
      "Ambani Afrika", "Best Educational Solution", "Mukundi Lambani", 2021);
  print("The App name is ");
  print(ambani.appName);
  print(" ");
  print("The developer of the App is: ");
  print(ambani.developer);
  print(" ");
  print("The App is in the category: ");
  print(ambani.sector);
  print(" ");
  print("The App won the  MTN Business App of the year award in the year ");
  print(ambani.year);
  print(" ");
  print("App name in all capital letters: ");
  print(ambani.allCap());
}
